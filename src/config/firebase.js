import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyClo7ox2C1eCBVdLJzemhBko_8xDWpuFnQ",
  authDomain: "chousei-firebase-f34a7.firebaseapp.com",
  databaseURL: "https://chousei-firebase-f34a7.firebaseio.com",
  projectId: "chousei-firebase-f34a7",
  storageBucket: "",
  messagingSenderId: "573789415039",
  appId: "1:573789415039:web:d2d26e22646942f7"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
